const { Router } = require('express');
const candidate = require('./controller/CandidateController');

var router = Router();

//Candidate
router.get('/candidate', candidate.findAll);
router.get('/candidate/:id', candidate.findOne);
router.post('/candidate', candidate.create);
router.put('/candidate/:id', candidate.update);
router.delete('/candidate/:id', candidate.delete);

module.exports = router;