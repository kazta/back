require('dotenv').config();
var express = require('express');
var router = require('./routes');
const cors = require('cors');

//init
const app = express();
const db = require('./models')
const config = require('./config');
db.sequelize.sync();

//server config
app.set('port', 80);
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors(config.application.cors.server))

//routes
app.use('/api', router);

app.listen(app.get('port'), () => {
    console.log('Server run on port', app.get('port'));
});