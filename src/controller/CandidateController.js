const { candidate } = require("../models");
const db = require("../models");
const Candidate = db.candidate;
const Op = db.Sequelize.Op;

// add new candidate
exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    //get candidate
    const candidate = {
        name: req.body.name,
        birthday: req.body.birthday,
        experience: req.body.experience
    };
    //save candidate
    Candidate.create(candidate).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the candidate."
        });
    });
};

// get all candidates
exports.findAll = (req, res) => {
    Candidate.findAll().then(data => {
        res.send(data);
    }).catch(err => {
        res.status(400).send({
            message:
                err.message || "Some error occurred while creating the candidate."
        });
    });
};

// get a candidate by id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Candidate.findByPk(id).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: "Error retrieving candidate with id=" + id
        });
    });
};

// update a candidate by id
exports.update = (req, res) => {
    const id = req.params.id;

    Candidate.update(req.body, {
        where: { id: id }
    }).then(num => {
        if (num == 1) {
            res.send({
                message: "Candidate was updated successfully."
            });
        } else {
            res.send({
                message: `Cannot update candidate with id=${id}. Maybe candidate was not found or req.body is empty!`
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: "Error updating candidate with id=" + id
        });
    });

};

// delete a candidate by id
exports.delete = (req, res) => {
    const id = req.params.id;

    Candidate.destroy({
        where: { id: id }
    }).then(num => {
        if (num == 1) {
            res.send({
                message: "Candidate was deleted successfully!"
            });
        } else {
            res.send({
                message: `Cannot delete candidate with id=${id}. Maybe candidate was not found!`
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: "Could not delete candidate with id=" + id
        });
    });
};

// delete all candidates
exports.deleteAll = (req, res) => {

};