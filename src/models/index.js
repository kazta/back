const dbSetting = require('../database/connection');
const { Sequelize, Model, DataTypes } = require('sequelize');

const sequelize = new Sequelize(dbSetting.DB, dbSetting.USER, dbSetting.PASSWORD, {
    host: dbSetting.HOST,
    dialect: dbSetting.dialect,
    operatorsAliases: false
});

const db = {}

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.candidate = require('./Candidate')(sequelize, Sequelize);

module.exports = db;